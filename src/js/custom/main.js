$(document).ready(function() {

  $("#sidebar").load("include/sidebar.html"); 

  // Sidebar Things
  var overlay = $('.sidebar-overlay');
  $('.sidebar-toggle').on('click', function() {
    var sidebar = $('#sidebar');
    sidebar.toggleClass('open');
    if ((sidebar.hasClass('sidebar-fixed-left') || sidebar.hasClass('sidebar-fixed-right')) && sidebar.hasClass('open')) {
      overlay.addClass('active');
    } else {
      overlay.removeClass('active');
    }
  });
  $('#sidebar').addClass('sidebar-fixed-left closed');
  overlay.on('click', function() {
    $(this).removeClass('active');
    $('#sidebar').removeClass('open');
  });
});
