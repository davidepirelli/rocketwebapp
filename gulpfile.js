/**
* ** GULPFILE **
* @since 1.0.0
* @author Davide Pirelli
* @package underscores
*/

// PLUGINS
var gulp     = require('gulp'),
prefix       = require('gulp-autoprefixer'),
mincss       = require('gulp-uglifycss'),
uglify       = require('gulp-uglify'),
imagemin     = require('gulp-imagemin'),
rename       = require('gulp-rename'),
concat       = require('gulp-concat'),
sass         = require('gulp-sass'),
ignore       = require('gulp-ignore'),
rimraf       = require('gulp-rimraf'),
zip          = require('gulp-zip'),
cache        = require('gulp-cache'),
runSequence  = require('gulp-run-sequence'),
livereload   = require('gulp-livereload'),
replace      = require('gulp-replace'),
util         = require('gulp-util'),
prompt       = require('gulp-prompt');
inject       = require('gulp-inject');

// DEV TASKS
gulp.task('scss', function(){
  return gulp.src('src/sass/main.scss')
  .pipe(inject(gulp.src(['src/sass/**/_*.scss'], {read:false}),{
    starttag: '/* inject:imports */',
    endtag: '/* endinject */',
    transform: function (filepath) {
      return '@import ".' + filepath + '";';
    }
  }))
  .pipe(gulp.dest('src/sass/'))
  .pipe(sass().on('error',sass.logError))
  .pipe(prefix('last 2 version', '> 1%'))
  .pipe(mincss())
  .pipe(concat('css/style.css'))
  .pipe(gulp.dest(''))
  .pipe(livereload());
});

gulp.task('php', function(){
  return gulp.src('*.php')
  .pipe(livereload());
});

gulp.task('html', function(){
  return gulp.src('*.html')
  .pipe(livereload());
});


gulp.task('vendorJs', function(){ //Concat and minify vendor scripts
  return gulp.src('src/js/vendor/**/*.js')
  .pipe(concat('vendors.js'))
  .pipe(rename({
    basename: 'vendor',
    suffix: '.min'
  }))
  .pipe(uglify())
  .pipe(gulp.dest('js'))
  .pipe(livereload());
});

gulp.task('customJs', function(){ //Concat and minify custom built scripts
  return gulp.src('src/js/custom/**/*.js')
  .pipe(concat('custom.js'))
  .pipe(rename({
    basename: 'custom',
    suffix: '.min'
  }))
  .pipe(uglify())
  .pipe(gulp.dest('js'))
  .pipe(livereload());
});

gulp.task('img', function(){
  return gulp.src('src/img/**/*.{jpg,png,gif,svg}')
  .pipe(cache(imagemin({optimizationLevel: 7, progressive: true, interlaced: true})))
  .pipe(gulp.dest('img'))
  .pipe(livereload());
});

gulp.task('watch', function() {
  livereload.listen({quiet: false});
  gulp.watch('src/sass/**/_*.scss',['scss']);
  gulp.watch('src/js/custom/**/*.js',['customJs']);
  gulp.watch('src/js/vendor/**/*.js',['vendorJs']);
  gulp.watch('src/img/**/*.{jpg,png,gif,svg}',['img']);
  gulp.watch('*.php',['php']);
  gulp.watch('*.html',['html']);
});

gulp.task('default', ['scss', 'html', 'php', 'vendorJs', 'customJs', 'img', 'watch']);

// BUILD TASKS
gulp.task('clear', function () {
  cache.clearAll();
});

gulp.task('buildFiles', function() {
  return gulp.src(build_src)
  .pipe(gulp.dest(build_url));
});

gulp.task('package', function () {
  return gulp.src(build_url+'/**/')
  .pipe(zip(proj_name+'.zip'))
  .pipe(gulp.dest('_build/'));
});

gulp.task('replace', function(){
  return gulp.src([build_url+'/**/*'])
  .pipe(prompt.prompt({
    type: 'input',
    name: 'thname',
    message: 'Name your theme: '
  }, function(res){
    return gulp.src([build_url+'/**/*'])
    .pipe(replace("'_s'",res.thname)) //textdomain
    .pipe(replace('_s_',res.thname)) //functionnames
    .pipe(replace(' _s',res.thname)) //DocBlocks
    .pipe(replace('_s-',res.thname)) //prefixed handles
    .pipe(gulp.dest(build_url));
  }));
});

gulp.task('build', function(cb) {
  runSequence('clear', 'scss', 'vendorJs', 'customJs', 'img', 'clear', 'buildFiles', 'replace', 'package', cb);
});
